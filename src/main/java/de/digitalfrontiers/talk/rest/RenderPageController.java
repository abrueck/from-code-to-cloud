package de.digitalfrontiers.talk.rest;

import de.digitalfrontiers.talk.exceptions.IllegalPageNumberException;
import de.digitalfrontiers.talk.service.DocumentRenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class RenderPageController {

    private final DocumentRenderService documentRenderService;

    @PostMapping(value = "/render",
            produces = MediaType.IMAGE_PNG_VALUE
    )
    public BufferedImage renderDocument(@RequestParam("document")
                                                    MultipartFile fileToRender) throws IOException {
        return documentRenderService.renderToPNG(fileToRender.getInputStream());
    }

    @PostMapping(value = "/render/{pageNr}",
            produces = MediaType.IMAGE_PNG_VALUE
    )
    public BufferedImage renderDocument(@RequestParam("document")
                                                MultipartFile fileToRender,
                                        @PathVariable("pageNr")
                                                int pageNr) throws IOException {
        return documentRenderService.renderToPNG(fileToRender.getInputStream(), pageNr);
    }

    @ExceptionHandler(IllegalPageNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalPageNumberException(IllegalPageNumberException e){
        return e.getMessage();
    }
}
