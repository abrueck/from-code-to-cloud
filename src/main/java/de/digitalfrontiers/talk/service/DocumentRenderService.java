package de.digitalfrontiers.talk.service;

import de.digitalfrontiers.talk.exceptions.IllegalPageNumberException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

@Service
public class DocumentRenderService {

    public BufferedImage renderToPNG(InputStream fileToRender) throws IOException {
        PDDocument pdDocument = PDDocument.load(fileToRender);
        PDFRenderer renderer = new PDFRenderer(pdDocument);
        return renderer.renderImage(0);
    }

    public BufferedImage renderToPNG(InputStream fileToRender, int pageNr) throws IOException {
        PDDocument pdDocument = PDDocument.load(fileToRender);

        if(pageNr > pdDocument.getNumberOfPages()){
            throw new IllegalPageNumberException("Given page number is out of range.");
        }

        PDFRenderer renderer = new PDFRenderer(pdDocument);
        return renderer.renderImage(pageNr);
    }
}
