package de.digitalfrontiers.talk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FromCodeToCloudSampleProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FromCodeToCloudSampleProjectApplication.class, args);
	}

}
